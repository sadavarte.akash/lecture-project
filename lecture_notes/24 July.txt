Data Passing

Parent Main-Component
	Child (Parent) Article-List
		Child(Parent) Article-Card
			Child Save Button - Save Function - Article Service
	

Input/Output (Parent's Data is getting shared with Child)
@Input() data = {};

@Output() delete_button_clicked = new EventEmitter()

Services
Data is shared across all the components that import it  / in which service is injected.
Service gets injected in a component and then we can use it.
Data - Variables
Common Functions


Observable = 
Variable zo change hoto, 
and it indicates/signals, to all the subscribers, that it has changed.
Subscribe and Perform some action = Observable


Query Params = URL Based Data Passing

Routing Params = Question mark cha pudhe variables pass na karta, passed data is part of url

How does data get shared or transfered from one coomponent to another in angular?
1. Input/Output
2. Services
3. Routing Params / Query Params
4. Observables
5. Local Storage / Session Storage


CRUD

Article Application
List of Articles / Array ko use karke / List dikhao - Service madhe store kara *articles*
When you click on an article, navigate to it's detail page.
Display information of clicked article on detail page
Deletion of Article
Creation of Article


Article Form Page
Article List Page - Delete
Article Card
Article Detail Page - Delete
Article Service - articles variable (array)


CRUD
Create - Form
Read - Read List/Read Individual
Update - Form with Pre filled Values
Delete - Button - Which deletes the item from list

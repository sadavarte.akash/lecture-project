import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'akash-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor(public jayesh:Router) { }
  ngOnInit(): void {
  }
  navigate(path,data){
    this.jayesh.navigate([path],{queryParams:{type:data}})
  }
}

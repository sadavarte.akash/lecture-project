import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  user = {
    first_name: "Jayesh",
    last_name: "Mahajan",
    employee_id: 12345,
    designation: "Software Developer"
  }

  constructor() { }

  add(num1, num2) {
    console.log(num1 + num2, 'Were the numbers added');
  }

}

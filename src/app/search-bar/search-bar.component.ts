import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../global.service';

@Component({
  selector: 'akash-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  constructor(public gs: GlobalService, public akash: ActivatedRoute) { }

  ngOnInit(): void {
    this.akash.queryParams.subscribe(data => {
      console.log('SEARCH BAR SUBSCRIPTION WAS CALLED')
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'akash-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  inner_articles = articles //innerArticles
  params: any = {

  }
  constructor(public ar: ActivatedRoute) {
    console.log('ARTICLE LIST CONSTRUCTOR WAS CALLED')
  }

  ngOnInit(): void {
    console.log('ARTICLE LIST NGONINIT WAS CALLED IN ARTICLE LIST')
    this.ar.queryParams.subscribe((param) => {
      console.log('ARTICLE LIST SUBSCRIPTION WAS CALLED')
      this.params = param
    })
    console.log(this.params);
  }

  delete_from_list(position) {
    // console.log('Delete from list called in article list',position)
    this.inner_articles.splice(position, 1)
  }

  unwanted_function(position) {
    console.log('Bhai', position, 'par delete function call huela hai')
  }

}


let articles = [
  'What happens if you wake up at 3.30AM at night',
  'Pradeep says comparing products should be banned in websites, if possible ban flex and promote grid',
  'Very Few People are as great as Anand Adule - Says Donald Trump',
  'Aditi eats 320 Mangoes in a day, creates new breaking world record!',
  'Akshay seeks a explaination from god! says he will go on hunger strike till then'
]



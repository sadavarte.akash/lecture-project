import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'akash-form-lecture',
  templateUrl: './form-lecture.component.html',
  styleUrls: ['./form-lecture.component.css']
})
export class FormLectureComponent implements OnInit {



  main_form_variable: any = undefined;

  constructor(public fb: FormBuilder) {

  }

  ngOnInit(): void {
    this.main_form_variable = this.fb.group({
      username: '',
      password: '',
      contact_details: this.fb.group({
        phone_number: this.fb.control('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
        email_address: this.fb.control('', []),
      }),
      location_details: this.fb.group({
        city: '',
        state: '',
        country: ''
      })
    })
  }

}



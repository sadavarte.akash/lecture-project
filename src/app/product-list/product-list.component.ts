import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'akash-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  product_list: any[] = [
    {
      title: 'Dell Laptop',
      price: '38000'
    },
    {
      title: 'Lenove Laptop',
      price: '45000'
    },
    {
      title: 'Asus Laptop',
      price: '31000'
    },
    {
      title: 'ACER Laptop',
      price: '29000'
    },
    {
      title: 'Apple Laptop',
      price: '70000'
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

  delete(index){
    this.product_list.splice(index,1);
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

  transform(value: any): any {
    let date: any = new Date(value);
    let valid = !isNaN(date.valueOf());
    if (valid) {
      return date;
    }

    return new Date()
  }

}


// function add(){
//   let a = 5;
//   let b = 6;

//   console.log(a+b);
//   return;

//   a=10;
//   b=69;
//   console.log(b-a)
// }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { ArticleListComponent } from './article-list/article-list.component';
import { ProductListComponent } from './product-list/product-list.component';
import { FormLectureComponent } from './pages/form-lecture/form-lecture.component';

export const routes: Routes = [
  { path: 'form-lecture', component: FormLectureComponent },
  { path: 'article-list', component: ArticleListComponent },
  { path: '', redirectTo: 'article-list', pathMatch: 'full' },
  { path: 'product-list', component: ProductListComponent },
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})

export class AppRoutingModule {
}
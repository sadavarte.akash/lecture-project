import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GlobalService } from '../global.service';

@Component({
  selector: 'akash-article-card',
  templateUrl: './article-card.component.html',
  styleUrls: ['./article-card.component.css']
})
export class ArticleCardComponent implements OnInit {

  @Input() news_information = 'This is my information' //newsInformation
  @Input() index:any
  @Output() delete_button_clicked = new EventEmitter(); //deleteButtonClicked

  constructor(public gs:GlobalService) { }

  ngOnInit(): void {
  }

  delete() {
    // console.log('Delete button click hua bhai!! from article card')
    this.delete_button_clicked.emit();
  }

}

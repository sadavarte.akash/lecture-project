import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GlobalService } from '../global.service';

@Component({
  selector: 'akash-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {

  @Input() product_detail:any;
  @Output() product_delete = new EventEmitter()
  constructor( public gs:GlobalService ) { }

  ngOnInit(): void {
    console.log(this.gs.user);

  }

  delete(){
 this.product_delete.emit();
  }

  
}
